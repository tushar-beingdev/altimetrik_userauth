# User Auth Demo

## A SpringBoot application having APIs for user management

1. I have used SpringBoot 2.2.1 for boilerplate code and jdk 1.8
2. Project was developed in IntelliJ Idea Community Edition using Maven build system
3. OS: Mac OS, Mojave

### REST APIs for CRUD Operations on User Entity

1. The project is structured in multiple layers like Controller, Service, Data, Config etc
2. I have used one simple User entity and have provided CRUD Operations using APIs in the UserController class
3. These APIs connect to UserService and performs the operations
4. As mentioned in the requirement documentation, I have also provided /import API to import users from an excel file (sample file in /data folder)
5. The application uses in memory H2 Database

### JWT Token based Authentication

1. All APIs except "/auth" are secured. These API need JWT token.
2. Use the "/auth" API to get the token. You will need to pass the username and password as request body of "/auth" API
3. For demo purposes any user can get the token provided the password is exactly the reverse of username
4. Example: username: "jack", password: "kcaj"
5. The "/auth" API should provide a valid JWT token. Use this token for accessing other APIs (/api/user)

### Multitenancy Support

1. As mentioned in the requirements, I have provided Multitenancy support at the SCHEMA level.
2. I have preinjected two schema representing two tenants viz ACME_INC & BLACKJACK_ORG (data.sql, schema.sql files)
3. Each request to the "api/user" API should have a header "X-TenantId" with value of either of the tenants mentioned above
4. Once the tenant is identified and set in the context, all of the database operations are executed for that tenant for that request.
