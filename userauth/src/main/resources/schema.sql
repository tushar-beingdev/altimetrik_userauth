create schema acme_inc;

create table acme_inc.user (
    id bigint not null identity,
    name varchar(255) not null,
    username varchar(255) not null unique,
    gender varchar (10),
    primary key(id)
);



create schema blackjack_org;

create table blackjack_org.user (
    id bigint not null identity,
    name varchar(255) not null,
    username varchar(255) not null unique,
    gender varchar (10),
    primary key(id)
);
