package com.altimetrik.playground.userauth.rest.controller;

import com.altimetrik.playground.userauth.data.entity.User;
import com.altimetrik.playground.userauth.rest.converter.impl.UserDtoConverter;
import com.altimetrik.playground.userauth.rest.dto.UserDto;
import com.altimetrik.playground.userauth.service.FileService;
import com.altimetrik.playground.userauth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

//TODO: Provide Swagger docs
@CrossOrigin
@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private FileService fileService;

    @Autowired
    private UserDtoConverter userDtoConverter;

    @GetMapping(path = "/{id}", produces = "application/json")
    public UserDto getUser(@PathVariable long id) {
        return userDtoConverter.convertToDto(userService.getUserById(id));
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<UserDto> addUser(@RequestBody UserDto newUser) {
        User user = userDtoConverter.convertToEntity(newUser);
        if (user == null) {
            return new ResponseEntity<>(newUser, HttpStatus.BAD_REQUEST);
        }

        User persisted = userService.addUser(user);
        return new ResponseEntity<>(userDtoConverter.convertToDto(persisted), HttpStatus.CREATED);
    }

    @PutMapping(consumes = "application/json")
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto existingUser) {
        User user = userDtoConverter.convertToEntity(existingUser);
        if (user == null) {
            return new ResponseEntity<>(existingUser, HttpStatus.BAD_REQUEST);
        }

        //Find whether this user exists in the database
        User persisted = userService.getUserById(user.getId());
        if (persisted == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(userDtoConverter.convertToDto(userService.updateUser(user)), HttpStatus.CREATED);
    }


    @DeleteMapping(consumes = "application/json")
    public ResponseEntity<String> deleteUser(@RequestBody UserDto existingUser) {
        User user = userDtoConverter.convertToEntity(existingUser);
        if (user == null) {
            return new ResponseEntity<>("Unable to parse request", HttpStatus.BAD_REQUEST);
        }

        //Find whether this user exists in the database
        User persisted = userService.getUserById(user.getId());
        if (persisted == null) {
            return new ResponseEntity<>("User with id: "+existingUser.getId()+" does not exist", HttpStatus.NOT_FOUND);
        }

        userService.deleteUser(user);
        return new ResponseEntity<>("User with id: "+existingUser.getId()+" deleted successfully", HttpStatus.OK);
    }

    @PostMapping(path = "/import")
    public ResponseEntity importUsers(MultipartFile file) throws IOException {
        List<User> users = fileService.parseUsers(file.getInputStream());
        userService.addAll(users);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }



}
