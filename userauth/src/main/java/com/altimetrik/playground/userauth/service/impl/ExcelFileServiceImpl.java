package com.altimetrik.playground.userauth.service.impl;

import com.altimetrik.playground.userauth.data.entity.User;
import com.altimetrik.playground.userauth.service.FileService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class ExcelFileServiceImpl implements FileService {

    @Override
    public List<User> parseUsers(InputStream usersFile) throws IOException{

        List<User> parsedUsers = new ArrayList<>();

        Workbook workbook = new XSSFWorkbook(usersFile);
        Sheet users = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = users.iterator();

        //Ignore first row as it is header
        rowIterator.next();

        //TODO: Use generic level validators for rows and cells.

        //Parse Data rows
        while (rowIterator.hasNext()) {

            //Create user for this row
            User user = new User();

            Row currentRow = rowIterator.next();
            Iterator<Cell> cellIterator = currentRow.iterator();

            while (cellIterator.hasNext()) {

                Cell currentCell = cellIterator.next();

                switch (currentCell.getColumnIndex()) {
                    case 0:
                        user.setName(currentCell.getStringCellValue());
                        break;

                    case 1:
                        user.setUserName(currentCell.getStringCellValue());
                        break;

                    case 2:
                        user.setGender(currentCell.getStringCellValue());
                        break;
                }

            }

            parsedUsers.add(user);

        }
        return parsedUsers;
    }
}
