package com.altimetrik.playground.userauth.service;

import com.altimetrik.playground.userauth.data.entity.User;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface FileService {

    List<User> parseUsers(InputStream usersFile) throws IOException;


}
