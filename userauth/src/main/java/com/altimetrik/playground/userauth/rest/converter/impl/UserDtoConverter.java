package com.altimetrik.playground.userauth.rest.converter.impl;

import com.altimetrik.playground.userauth.data.entity.User;
import com.altimetrik.playground.userauth.rest.converter.DtoConverter;
import com.altimetrik.playground.userauth.rest.dto.UserDto;
import org.springframework.stereotype.Service;

@Service
public class UserDtoConverter implements DtoConverter<UserDto, User> {

    @Override
    public User convertToEntity(UserDto userDto) {
        //TODO: Provide a validator
        return User.builder()
                .id(userDto.getId())
                .name(userDto.getName())
                .userName(userDto.getUserName())
                .gender(userDto.getGender())
                .build();
    }

    @Override
    public UserDto convertToDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .userName(user.getUserName())
                .gender(user.getGender())
                .build();
    }
}
