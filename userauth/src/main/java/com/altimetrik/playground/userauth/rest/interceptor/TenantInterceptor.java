package com.altimetrik.playground.userauth.rest.interceptor;

import com.altimetrik.playground.userauth.config.TenantContext;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Request interceptor to get the tenantId from request header
 */
@Component
public class TenantInterceptor extends HandlerInterceptorAdapter {

    private static final String TOKEN_HEADER = "X-TenantID";

    //Set the tenant in the context to be used further
    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) throws Exception {

        String tenantId = request.getHeader(TOKEN_HEADER);
        TenantContext.setCurrentTenant(tenantId);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object handler,
                           ModelAndView modelAndView) throws Exception {

        TenantContext.clear();
    }
}