package com.altimetrik.playground.userauth.service;

import com.altimetrik.playground.userauth.data.entity.User;

import java.util.List;

public interface UserService {

    User getUserById(Long id);

    User addUser(User newUser);

    User updateUser(User existingUser);

    void deleteUser(User existingUser);

    void addAll(List<User> users);
}
