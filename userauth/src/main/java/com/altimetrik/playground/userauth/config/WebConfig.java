package com.altimetrik.playground.userauth.config;

import com.altimetrik.playground.userauth.rest.interceptor.TenantInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getTenantInterceptor());
    }

    @Bean
    public TenantInterceptor getTenantInterceptor() {
        return new TenantInterceptor();
    }


}
