package com.altimetrik.playground.userauth.rest.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UserDto {

    private Long id;
    private String name;
    private String gender;
    private String userName;
}
