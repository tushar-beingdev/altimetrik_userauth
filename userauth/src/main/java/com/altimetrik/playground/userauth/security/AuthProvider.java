package com.altimetrik.playground.userauth.security;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom Authentication manager for validating the username and password.
 * Current implementation is for demo purposes.
 * Need to have prod level strategy for validation.
 */
public class AuthProvider implements AuthenticationManager {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String username = authentication.getPrincipal() + "";
        String password = authentication.getCredentials() + "";

        if(username.isEmpty() || password.isEmpty()) {
            throw new BadCredentialsException("User does not exist");
        }

        //Custom logic for demo purposes. - Checking whether the password is a reverse string of username
        //TODO: In prod environment, this should be done using proper database and validations

        StringBuilder reverseBuilder = new StringBuilder(username);

        if(!password.equalsIgnoreCase(reverseBuilder.reverse().toString())) {
            throw new BadCredentialsException("Invalid creds");
        }

        List<GrantedAuthority> grantedAuths = new ArrayList<>();
        grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));

        return new UsernamePasswordAuthenticationToken(username, password, grantedAuths);
    }
}
