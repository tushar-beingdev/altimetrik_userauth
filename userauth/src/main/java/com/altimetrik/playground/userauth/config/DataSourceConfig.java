package com.altimetrik.playground.userauth.config;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Configuration class for setting up the Datasource properties
 */
@Configuration
public class DataSourceConfig {
     
    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.h2.Driver");
        dataSourceBuilder.url("jdbc:h2:mem:~/altimetrik/test");
        dataSourceBuilder.username("sa");
        dataSourceBuilder.password(""); //For demo purposes
        return dataSourceBuilder.build();
    }
}