package com.altimetrik.playground.userauth.rest.converter;

public interface DtoConverter<Dto, Entity> {

    Entity convertToEntity(Dto dto);

    Dto convertToDto(Entity entity);

}
