package com.altimetrik.playground.userauth.service.impl;

import com.altimetrik.playground.userauth.data.entity.User;
import com.altimetrik.playground.userauth.data.repository.UserRepository;
import com.altimetrik.playground.userauth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;


    @Override
    public User getUserById(Long id) {
        //TODO: Validate
        Optional<User> byId = userRepository.findById(id);
        return byId.orElse(null);
    }

    @Override
    public User addUser(User newUser) {
        //TODO: Validate whether this user already exists in DB
        newUser.setId(null);
        return userRepository.save(newUser);
    }

    @Override
    public User updateUser(User existingUser) {
        User existing = userRepository.findById(existingUser.getId()).get();
        existing.setGender(existingUser.getGender());
        existing.setName(existing.getName());
        existing.setUserName(existingUser.getUserName());
        return userRepository.save(existing);
    }

    @Override
    public void deleteUser(User existingUser) {
        userRepository.deleteById(existingUser.getId());
    }

    @Override
    public void addAll(List<User> users) {
        if (users == null || users.isEmpty()) {
            return;
        }

        userRepository.saveAll(users);
    }
}
